const moogoose = require('mongoose');

let placeSchema = new moogoose.Schema({
    title:{
        type: String,
        required: true
    },
    description: String,
    acceptsCreditCard:{
        type: Boolean,
        default: false
    },
    coverImage:String,
    avatarImage:String,
    openHour: Number,
    closeHour: Number
});

let Place = moogoose.model('Place',placeSchema);

module.exports = Place;