const expres = require('express');
const bodyParser = require('body-parser');

const app = expres();

//Configurar BodyParser
app.use(bodyParser.json({})); //lectura de la petcion en Json 
app.use(bodyParser.urlencoded({extended:false})); //permite que le envien urlencoded


const places =[
    {
        'title': 'Odfie lecd',
        'description': 'Loresd ds',
        'address': 'JDde'
    },
    {
        'title': 'Odfie lecd',
        'description': 'Loresd ds',
        'address': 'JDde'
    },
    {
        'title': 'Odfie lecd',
        'description': 'Loresd ds',
        'address': 'JDde'
    }
]
app.get('/',(req,res)=>{
    res.json(places);
})
app.post('/',(req,res)=>{
    res.json(req.body.nombre);
})

//archivos estaticos
app.use(expres.static('public'));

app.listen(3000);